module.exports = function cacheFunction(cb) {
    const cache = {};
    return function (para) {
        if (!Object.prototype.hasOwnProperty.call(cache, para)) {
            cache[para] = cb(para);
            return cb(para);
        }
        else
            return { ["fetching from cache"]: cache[para] };
    }
}

