module.exports=  function limitFunctionCallCount(cb, n) {
    let end =0;
    return function (){
        if(end ===n){
            return null;
        }
        ++end;
        return cb();
    }
}