limitFunctionCallCount = require("../limitFunctionCallCount.js")
let count1 = 0;

function cb1() {
    count1++;
    return (count1);
}

let count2 = 0;

function cb2() {
    count2++;
    return (count2);
}

let lowerThanN = limitFunctionCallCount(cb1, 4);
let upperThanN = limitFunctionCallCount(cb2, 4);

lowerThanN();
lowerThanN();
upperThanN();
upperThanN();
upperThanN();
upperThanN();
upperThanN();
upperThanN();

let actualResult1 = lowerThanN();
let actualResult2 = upperThanN();
let expectedResult1 = 3;
let expectedResult2 = null;

if ((actualResult1 === expectedResult1) && (actualResult2 === expectedResult2)) {
    console.log(`lowerThanN invoked ${count1} times & upperThanN invoked ${count2} times`);
}
else {
    console.log("Results are different.");
}
