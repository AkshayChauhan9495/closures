const counterFactory = require('../counterFactory.js')
let result = counterFactory();
result.increment();
result.decrement();
result.decrement();
result.decrement();
result.decrement();
let finalResult = result.increment();
let expectedResult = -2;
if (finalResult === expectedResult) {
    console.log(finalResult);
}
else {
    console.log("Results are different.");
}